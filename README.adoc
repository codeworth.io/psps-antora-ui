= PSPS Antora UI
// Settings:
:experimental:
:hide-uri-scheme:
// Project URLs:
:url-project: https://gitlab.com/antora/antora-ui-default
:url-preview: https://antora.gitlab.io/antora-ui-default
:url-ci-pipelines: {url-project}/pipelines
:img-ci-status: {url-project}/badges/master/pipeline.svg
// External URLs:
:url-antora: https://antora.org
:url-antora-docs: https://docs.antora.org
:url-git: https://git-scm.com
:url-git-dl: {url-git}/downloads
:url-gulp: http://gulpjs.com
:url-opendevise: https://opendevise.com
:url-nodejs: https://nodejs.org
:url-nvm: https://github.com/creationix/nvm
:url-nvm-install: {url-nvm}#installation

image:{img-ci-status}[CI Status (GitLab CI), link={url-ci-pipelines}]

Antora UI for the PSPS project.

== Copyright and License
Use of this software is granted under the terms of the https://www.mozilla.org/en-US/MPL/2.0/[Mozilla Public License Version 2.0] (MPL-2.0).
See link:LICENSE[] to find the full license text.

== Authors
CodeWorth.io team, based on https://gitlab.com/antora/antora-ui-default[Antora Default UI].
Development of Antora is led and sponsored by {url-opendevise}[OpenDevise Inc].
